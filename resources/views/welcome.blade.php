@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome in World of Poker...</div>

                <div class="panel-body">
                     <p>Become a king of PokerConqueror now!</p>
                     <p> 
                        <a   href="{{ url('/register') }}">
                             <button style= "width:150px;height:80px;" type="button" class="btn btn-primary">
                                    <i class="fa fa-btn fa-registered"></i> Start right now!
                                </button>
                        </a>
                         
                     </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
