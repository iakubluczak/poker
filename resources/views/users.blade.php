@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Users Table</div>
                    <div class="panel-body">
                        @foreach ($users as $user)
                            <p>{{$user->login}} <button type="button" class="btn btn-success">Zaproś do gry</button></p> 
                        @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection