<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoggedUsers extends Model
{
    protected $table = 'logged_users';
    
    public function getAllUsers(){
        
        return $this->get();
    }
    
     public function insertUser($email, $login){
        $this->insert([
            ['email' => '$email', 'login' => $login]
            ]);
    }
}