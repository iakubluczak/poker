#!/usr/bin/python
# -*- coding: utf-8 -*-
import os, sys
from selenium import webdriver
import datetime


def laravel():
    driver = webdriver.Chrome('./chromedriver/chromedriver.exe')
    driver.maximize_window()
    driver.get("https://software-engeenering-olejs.c9users.io/")
    driver.find_element_by_xpath("//a[@class='solid fat info button']").click()

    driver.find_element_by_xpath("//*[@id='app-layout']//a[@class='navbar-brand']").click()
    driver.find_element_by_xpath("//a//button[ @type = 'button'][@class='btn btn-primary']").click()
    return driver

def home():
    driver = webdriver.Chrome('./chromedriver/chromedriver.exe')
    driver.maximize_window()
    driver.get("https://software-engeenering-olejs.c9users.io/")
    driver.find_element_by_xpath("//a[@class='solid fat info button']").click()

    driver.find_element_by_xpath("// *[contains(text(), 'Home')]").click()
    return driver


if __name__ == "__main__":
    t = datetime.datetime.today()
    print("It's ", t)
    driver = laravel()
    driver.close()
    driver = home()
    # driver.close()
    # driver = register("to@gmail.com", "malego",remember=True)
