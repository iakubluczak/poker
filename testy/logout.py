#!/usr/bin/python
# -*- coding: utf-8 -*-
import os, sys
from selenium import webdriver
import datetime
from login import login

def logout( password, email):
    driver = login(email,password,True)
    driver.find_element_by_xpath("//a[@class='dropdown-toggle'][@role='button']").click()
    driver.find_element_by_xpath("//a[contains(text(),'Logout')]").click()
    return driver

if __name__ == "__main__":
    t = datetime.datetime.today()
    print("It's ", t)
    driver = logout( email="to@gmail.com", password="malego")
