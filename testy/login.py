#!/usr/bin/python
# -*- coding: utf-8 -*-
import os, sys
from selenium import webdriver
import datetime


def login(email, password, remember):
    driver = webdriver.Chrome('./chromedriver/chromedriver.exe')
    driver.maximize_window()
    driver.get("https://software-engeenering-olejs.c9users.io/")

    driver.find_element_by_xpath("//a[@class='solid fat info button']").click()
    driver.find_element_by_xpath("//*[@id='app-navbar-collapse']/ul[2]//*[contains(text(),'Login')]").click()
    driver.find_element_by_id("email").send_keys(email)
    driver.find_element_by_id("password").send_keys(password)

    if remember:
        driver.find_element_by_xpath("//input[@type='checkbox'][@name='remember']").click()

    driver.find_element_by_xpath("//*[@id='app-layout']//button[@type='submit']").click()
    return driver


if __name__ == "__main__":
    t = datetime.datetime.today()
    print("It's ", t)
    driver = login("to@gmail.com", "malego",remember=False)
    # driver.close()
    # driver = register("to@gmail.com", "malego",remember=True)
