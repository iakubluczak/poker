#!/usr/bin/python
# -*- coding: utf-8 -*-
import os, sys
import datetime

from login import login
from logged_users import logged_users
from home import home, laravel
from logout import logout
from register import register


if __name__ == "__main__":
    t = datetime.datetime.today()
    print("It's ", t , "register")
    driver = register(username="janek",email="to@gmail.com", password="malego")
    driver.close()
    print("It's ", t , "login True")
    driver = login(email="to@gmail.com", password="malego", remember=True)
    driver.close()
    print("It's ", t, "login False")
    driver = login(email="to@gmail.com", password="malego", remember=False)
    driver.close()
    print("It's ", t , "logout")
    driver = logout(email="to@gmail.com", password="malego")
    driver.close()
    print("It's ", t , "Home")
    driver = home()
    driver.close()
    print("It's ", t, "Laravel")
    driver = laravel()
    driver.close()
    print("It's ", t, "logged users")
    driver = logged_users(password="malego", email="to@gmail.com")
    driver.close()
