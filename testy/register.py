#!/usr/bin/python
# -*- coding: utf-8 -*-
import os, sys
from selenium import webdriver
import random
import time
import datetime


def alert_every_n_seconds(future, n):
    print("Building at", future)
    while future > datetime.datetime.today():
        t = datetime.datetime.today()
        seconds = min((future - t).total_seconds(), n)
        print("sleeping for ", time.strftime('%H:%M:%S', time.gmtime(seconds)))
        time.sleep(seconds)
        print(datetime.datetime.today(), "remaining",
              time.strftime('%H:%M:%S', time.gmtime((future - datetime.datetime.today()).total_seconds())))
        if seconds < n:
            break


def register(username, email, password):
    driver = webdriver.Chrome('./chromedriver/chromedriver.exe');
    driver.maximize_window()
    driver.get("https://software-engeenering-olejs.c9users.io/")

    driver.find_element_by_xpath("//a[@class='solid fat info button']").click()
    driver.find_element_by_xpath("//*[@id='app-navbar-collapse']/ul[2]//*[contains(text(),'Register')]").click()
    driver.find_element_by_id("name").send_keys(username)
    driver.find_element_by_id("email").send_keys(email)
    driver.find_element_by_id("password").send_keys(password)
    driver.find_element_by_id("password-confirm").send_keys(password)

    driver.find_element_by_xpath("//*[@id='app-layout']//button[@type='submit']").click()
    return driver


if __name__ == "__main__":
    t = datetime.datetime.today()
    # delta = datetime.timedelta(minutes=7, seconds=1)
    # alert_every_n_seconds(future, 150)
    # t = datetime.datetime.today()
    print("It's ", t)
    driver = register("janek", "to@gmail.com", "malego")
    # try:
    #     driver.find_element_by_xpath("//a[@class='popup_box_close']").click()
    #     driver.find_element_by_xpath("//button[@class='evt-confirm-btn]").click()
    #     print("closing campaign popup")
    # except:
    #     pass
    # try:
    #     driver.find_element_by_xpath("//a[@class='chat-button-close']").click()
    #     print("closing chat")
    # except:
    #     pass
    # driver.find_element_by_xpath(
    #     "//table[@id='buildings']//tr[@id='main_buildrow_smith']//a[@class='btn btn-build']").click()
